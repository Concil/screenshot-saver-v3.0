
____________________________________________________
programm name: Screenshot Saver
version: 2.0
developer: Florian 'Concil' Schmidt
copyright: Florian 'Concil' Schmidt & Just4Gaming
____________________________________________________

__information__

press the general "print" button on your keyboard minimal 
2 seconds long to make an screenshot from your desktop.

to change the save folder go with your mouse right click 
on the "scsh_Saver.exe" icon in the tray menu and
select "About / Settings" after that click again 
on "settings" button in the About Window for open the 
settings window

warning: it will be automatically created a file called "settings.ini" in the same folder where the application is.

note: if you have open the settings window the script is 
paused and will wait for close the window so you can't make
screenshot if the settings window is opened


visit www.just4gaming.de for help. [GERMAN]



