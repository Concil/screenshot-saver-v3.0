#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <GuiIPAddress.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#Region ### START Koda GUI section ### Form=settingsform.kxf
$settingsform = GUICreate("settings", 289, 517, -1, -1, -1, BitOR($WS_EX_TOOLWINDOW,$WS_EX_WINDOWEDGE))
$folderpath = GUICtrlCreateInput("folderpath", 7, 27, 194, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_READONLY))
$openfolderbrowser = GUICtrlCreateButton("Choose", 199, 25, 75, 25)
$Label1 = GUICtrlCreateLabel("Save file to:", 7, 9, 60, 17)
$Label2 = GUICtrlCreateLabel("Interval how long the Capture Form is showing on:", 9, 66, 239, 17)
$interval = GUICtrlCreateInput("interval", 9, 84, 85, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
$savefile = GUICtrlCreateButton("save file", 77, 486, 134, 26)
$autostart = GUICtrlCreateCheckbox("use autostart with Windows", 9, 138, 262, 17)
$ftp_module = GUICtrlCreateCheckbox("use FTP Module to upload files automaticlly", 9, 162, 259, 17)
$Group1 = GUICtrlCreateGroup(" FTP Settings ", 11, 188, 257, 295, BitOR($GUI_SS_DEFAULT_GROUP,$BS_CENTER,$WS_CLIPSIBLINGS))
$Label3 = GUICtrlCreateLabel("host ip:", 71, 211, 38, 17)
$ipadress = _GUICtrlIpAddress_Create($settingsform, 71, 229, 130, 19)
_GUICtrlIpAddress_Set($ipadress, "0.0.0.0")
$username = GUICtrlCreateInput("username", 30, 287, 213, 21)
$Label4 = GUICtrlCreateLabel("username:", 30, 267, 53, 17)
$password = GUICtrlCreateInput("password", 30, 340, 213, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_PASSWORD))
$Label5 = GUICtrlCreateLabel("password:", 30, 318, 52, 17)
$ftp_input_folder = GUICtrlCreateInput("ftp_input_folder", 31, 397, 213, 21)
$Label6 = GUICtrlCreateLabel("ftp folder ( Upload File )", 33, 378, 113, 17)
$ftp_input_linkurl = GUICtrlCreateInput("ftp_input_linkurl", 31, 452, 213, 21)
$Label7 = GUICtrlCreateLabel("Link url to folder on your FTP Server", 31, 430, 174, 17)
GUICtrlCreateGroup("", -99, -99, 1, 1)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit

		Case $openfolderbrowser
		Case $interval
		Case $savefile
		Case $autostart
	EndSwitch
WEnd
