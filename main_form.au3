#include <GUIConstantsEx.au3>
#include <ProgressConstants.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#Region ### START Koda GUI section ### Form=main.kxf
$Form1 = GUICreate("Form1", 622, 121, 621, 246, BitOR($WS_MINIMIZEBOX,$WS_POPUP,$WS_GROUP))
GUISetFont(17, 400, 0, "MS Sans Serif")
GUISetBkColor(0x0066CC)
$titel = GUICtrlCreateLabel("text-file", 137, 11, 476, 33)
GUICtrlSetFont(-1, 18, 800, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0x000000)
$bild_save_label = GUICtrlCreateLabel("Images was saved in ", 138, 42, 475, 46)
GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0x000000)
$Pic1 = GUICtrlCreatePic("", 24, 12, 100, 100)
$Progress1 = GUICtrlCreateProgress(378, 95, 236, 17)
$Label1 = GUICtrlCreateLabel("Upload File:", 286, 91, 88, 24)
GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFFFFFF)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit

	EndSwitch
WEnd
