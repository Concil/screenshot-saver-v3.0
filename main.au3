; -> INCLUDING <-
;___________________________________________________________________

#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <ScreenCapture.au3>
#include <GuiIPAddress.au3>
#include <WinAPI.au3>
#include <Misc.au3>
#Include <GDIPlus.au3>
#include <GuiButton.au3>
#include <GuiImageList.au3>
#include <EditConstants.au3>
#include "FTP.au3"

;___________________________________________________________________

; -> MAIN SETTINGS <-
;___________________________________________________________________

Opt("TrayIconDebug", 0)
Opt("TrayIconHide", 0)
Opt("TrayMenuMode", 3)

Global $ProgrammName = "Screenshot Saver"
Global $ProgrammVersion = "3.0"
Global $ProgrammSettingsFile = @ScriptDir & "\settings.ini"

Global $ABOUTICON = @TempDir & "\screen_saver_tool_icon.ico"
Global $BANNERICON = @TempDir & "\screen_saver_tool_tool_banner.jpg"

FileInstall("D:\Source - Programme\Screenshot Saver\icon.ico", @TempDir & "\screen_saver_tool_icon.ico", 1)
FileInstall("D:\Source - Programme\Screenshot Saver\tool_banner.jpg", @TempDir & "\screen_saver_tool_tool_banner.jpg", 1)

__LoadConfig()

$userdll = DllOpen("user32.dll")

AdlibRegister ("Main", 250)

Local $iAbout = TrayCreateItem("About / Settings")
TrayCreateItem("")
Local $iExit = TrayCreateItem("Exit")
	
	
While 1
   SwitchCaseTray()   
WEnd
;___________________________________________________________________


; -> FUNCTIONS <-
;___________________________________________________________________

Func _RegAddAdmin($KeyName, $ValueName, $Type, $Value)
    Local $Return
    If Not IsAdmin() Then
        $Return = ShellExecuteWait("reg.exe",'ADD "' & $KeyName & '" /v "' & $ValueName & '" /t "' & $Type & '" /d "' & StringReplace($Value,'"','"') & '" /f',@SystemDir,"runas",1)
        Return SetError(@error,@extended,$Return)
    EndIf
    $Return = RegWrite($KeyName,$ValueName,$Type,$Value)
    Return SetError(@error,@extended,$Return)
EndFunc

Func _IsChecked($iControlID)
    Return BitAND(GUICtrlRead($iControlID), $GUI_CHECKED) = $GUI_CHECKED
EndFunc   ;==>_IsChecked

Func __LoadConfig()
   Global $SettingsFile[9];
   $SettingsFile[0] = IniRead( $ProgrammSettingsFile, "core", "savepicture", @DesktopDir)
   $SettingsFile[1] = IniRead( $ProgrammSettingsFile, "core", "showcapturewindow", 2000 )   
   $SettingsFile[2] = IniRead( $ProgrammSettingsFile, "core", "autostart", 0 )   
   $SettingsFile[3] = IniRead( $ProgrammSettingsFile, "ftp", "module", 0 )   
   $SettingsFile[4] = IniRead( $ProgrammSettingsFile, "ftp", "ip", 0 )   
   $SettingsFile[5] = IniRead( $ProgrammSettingsFile, "ftp", "username", 0 )   
   $SettingsFile[6] = IniRead( $ProgrammSettingsFile, "ftp", "password", 0 )   
   $SettingsFile[7] = IniRead( $ProgrammSettingsFile, "ftp", "folder", 0 )   
   $SettingsFile[8] = IniRead( $ProgrammSettingsFile, "ftp", "linkurl", 0 )   
EndFunc

Func SwitchCaseTray()   
   Switch TrayGetMsg()
	  Case $iAbout
		 AboutWindow()
	  Case $iExit
		 Exit
   EndSwitch 
EndFunc

Func Main() 
   If _IsPressed("2C", $userdll) Then
	  AdlibUnRegister("Main")
	  $filename = @YEAR & "-" & @MON & "-" & @MDAY & "__" & @HOUR & "-" & @MIN & "-" & @SEC & ".jpg"
	  $folder = $SettingsFile[0]
	  $hBmp = _ScreenCapture_Capture("")
	  _ScreenCapture_SaveImage($SettingsFile[0] & "\" & $filename, $hBmp)	  
	  CaptureScreen($filename, $SettingsFile[0])
   EndIf
EndFunc

Func CaptureScreen($name, $folder)
   _GDIPlus_Startup ()
   $hImage = _GDIPlus_ImageLoadFromFile($folder & "\" & $name)
   If @error Then
	  MsgBox(16, "Error", "Does the file exist?")
	  Exit 1
   EndIf
   $imagewidth = _GDIPlus_ImageGetWidth($hImage)
   $imagegheight = _GDIPlus_ImageGetWidth($hImage)
   $iFileSize = FileGetSize($folder & "\" & $name)
   $sizetext = "size: " & $iFileSize & " (" & $imagewidth & "x" & $imagegheight & ") "
   _GDIPlus_ImageDispose ($hImage)
   _GDIPlus_ShutDown ()   
   
   $CaptureScreen = GUICreate("- SAVED -" & $ProgrammName , 622, 121, @DesktopWidth-622-20, @DesktopHeight-121-60, BitOR($WS_MINIMIZEBOX,$WS_POPUP,$WS_GROUP))
   GUISetFont(17, 400, 0, "MS Sans Serif")
   GUISetBkColor(0x0066CC)
   
   $titel = GUICtrlCreateLabel($name, 137, 11, 476, 33)
   GUICtrlSetFont(-1, 18, 800, 0, "MS Sans Serif")
   GUICtrlSetColor(-1, 0x000000)
   
   $bild_save_label = GUICtrlCreateLabel("saved to: " & $folder & @CRLF & $sizetext, 138, 42, 475, 46)
   GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
   GUICtrlSetColor(-1, 0x000000)
   
   $Pic1 = GUICtrlCreatePic($SettingsFile[0] & "\" & $name, 24, 12, 100, 100)
   
   $Progress1 = GUICtrlCreateProgress(378, 95, 236, 17)
   
   $Label1 = GUICtrlCreateLabel("Upload File:", 286, 91, 88, 24)
   GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
   GUISetState(@SW_SHOW)
   
   
   __UploadToFTP($SettingsFile[0] & "/" & $name, $name)
   GUICtrlSetData($Progress1, 100)
   
   Sleep($SettingsFile[1]) 
   
   GUIDelete($CaptureScreen)
   AdlibRegister ("Main", 250)
EndFunc

Func __UploadToFTP($filename, $name)
   __LoadConfig()   
   $Open = _FTPOpen('FileUpload')
   $Conn = _FTPConnect($Open, $SettingsFile[4], $SettingsFile[5], $SettingsFile[6])
   $Ftpp = _FtpPutFile($Conn, $filename, $SettingsFile[7] & "/" & $name)
   ConsoleWrite($filename)
   ConsoleWrite($name)
   $Ftpc = _FTPClose($Open)

   ClipPut($SettingsFile[8] & "/" & $name)
EndFunc

Func AboutWindow() 
   #Region ### START Koda GUI section ### Form=about.kxf
	  $about = GUICreate("About - " & $ProgrammName , 615, 249, -1, -1, -1, BitOR($WS_EX_TOOLWINDOW,$WS_EX_WINDOWEDGE))
	  $Icon1 = GUICtrlCreateIcon($ABOUTICON, -1, 9, 119, 90, 90)
	  $Label1 = GUICtrlCreateLabel("Screenshot Saver ", 105, 128, 166, 29)
	  GUICtrlSetFont(-1, 15, 400, 0, "MS Sans Serif")
	  GUICtrlSetColor(-1, 0xB6181D)
	  
	  $Label2 = GUICtrlCreateLabel("Developer: Florian 'Concil' Schmidt | Version: " & $ProgrammVersion, 115, 159, 400, 17)
	  
	  $Pic1 = GUICtrlCreatePic($BANNERICON, 0, 0, 615, 102)
	  
	  $Label3 = GUICtrlCreateLabel("Website & Support: ", 115, 179, 98, 17)
	  
	  $Label4 = GUICtrlCreateLabel("www.just4gaming.de", 213, 178, 120, 17)
	  GUICtrlSetFont(-1, 8, 800, 4, "MS Sans Serif")
	  GUICtrlSetColor(-1, 0x0000FF)
	  GUICtrlSetCursor (-1, 0)
	  
	  $information = GUICtrlCreateGroup(" information ", 367, 122, 221, 112)
	  $Label5 = GUICtrlCreateLabel("to save an image press the 'print' button " & @CRLF & "on your keyboard!" , 379, 140, 197, 60)
	  GUICtrlCreateGroup("", -99, -99, 1, 1)
	  
	  $okay_button = GUICtrlCreateButton("Okay", 14, 214, 85, 25)
	  
	  $settings = GUICtrlCreateButton("settings", 121, 214, 134, 25)
	  
	  GUISetState(@SW_SHOW)
	  While 2
		 Switch GUIGetMsg()	
			Case $GUI_EVENT_CLOSE, $okay_button
			   GUISetState(@SW_HIDE, $about)
			Case $Label4
			   ShellExecute("http://www.just4gaming.de")
			Case $settings
			   GUIDelete($about)
			   __ShowSettingsForm()
		 EndSwitch
		 SwitchCaseTray()  
	  WEnd
   #EndRegion ### END Koda GUI section ###   
EndFunc

func __ShowSettingsForm() 
   AdlibUnRegister("Main")
   __LoadConfig()
   ;$SettingsFile[0] -> folder path
   ;$SettingsFile[1] -> interval to show the display
   #Region ### START Koda GUI section ### Form=settingsform.kxf
	  $settingsform = GUICreate("settings", 289, 517, -1, -1, -1, BitOR($WS_EX_TOOLWINDOW,$WS_EX_WINDOWEDGE))
	  
	  $folderpath = GUICtrlCreateInput($SettingsFile[0], 7, 27, 194, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_READONLY))
	  
	  $openfolderbrowser = GUICtrlCreateButton("choose", 199, 25, 75, 25)
	  
	  $Label1 = GUICtrlCreateLabel("save file to:", 7, 9, 60, 17)
	  
	  $Label2 = GUICtrlCreateLabel("interval how long the Capture Form is showing on:", 9, 66, 239, 17)
	  
	  $interval = GUICtrlCreateInput($SettingsFile[1], 9, 84, 85, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
	  
	  $autostart = GUICtrlCreateCheckbox("use autostart with Windows", 9, 138, 262, 17)
	  If $SettingsFile[2] == 1 Then
		 GUICtrlSetState($autostart, $GUI_CHECKED)
	  EndIf
	  
	  $ftp_module = GUICtrlCreateCheckbox("use FTP Module to upload files automaticlly", 9, 162, 259, 17)	  
	  $Group1 = GUICtrlCreateGroup(" FTP Settings ", 11, 188, 257, 260, BitOR($GUI_SS_DEFAULT_GROUP,$BS_CENTER,$WS_CLIPSIBLINGS))
	  
	  $Label3 = GUICtrlCreateLabel("host ip:", 71, 211, 38, 17)	  
	  $ftp_input_ipadress = GUICtrlCreateInput("", 71, 229, 130, 19)
	  
	  $Label4 = GUICtrlCreateLabel("username:", 30, 267, 53, 17)
	  $ftp_input_username = GUICtrlCreateInput("", 30, 287, 213, 21)
	  
	  
	  $Label5 = GUICtrlCreateLabel("password:", 30, 318, 52, 17)
	  $ftp_input_password = GUICtrlCreateInput("", 30, 340, 213, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_PASSWORD))	  
	  
	  $Label6 = GUICtrlCreateLabel("ftp folder ( Upload File )", 33, 378, 113, 17)
	  $ftp_input_folder = GUICtrlCreateInput("/htdocs/screencast/", 31, 397, 213, 21)
	  
	  $Label7 = GUICtrlCreateLabel("Link url to folder on your FTP Server", 31, 430, 174, 17)
	  $ftp_input_linkurl = GUICtrlCreateInput("http://images.just4gaming/concils_screencast/", 31, 452, 213, 21)
	  
	  GUICtrlCreateGroup("", -99, -99, 1, 1)
	  
	  If $SettingsFile[4] Then 
		 GUICtrlSetData($ftp_input_ipadress, $SettingsFile[4]) 
	  EndIf
	  If $SettingsFile[5] Then 
		 GUICtrlSetData($ftp_input_username, $SettingsFile[5]) 
	  EndIf
	  If $SettingsFile[6] Then 
		 GUICtrlSetData($ftp_input_password, $SettingsFile[6]) 
	  EndIf
	  If $SettingsFile[7] Then 
		 GUICtrlSetData($ftp_input_folder, $SettingsFile[7]) 
	  EndIf
	  If $SettingsFile[8] Then 
		 GUICtrlSetData($ftp_input_linkurl, $SettingsFile[8]) 
	  EndIf
	  
	  
	  GUICtrlCreateGroup("", -99, -99, 1, 1)
	  GUICtrlSetState($Group1, $GUI_HIDE)
	  GUICtrlSetState($ftp_input_username, $GUI_HIDE)
	  GUICtrlSetState($Label5, $GUI_HIDE)
	  GUICtrlSetState($Label4, $GUI_HIDE)
	  GUICtrlSetState($ftp_input_password, $GUI_HIDE)
	  GUICtrlSetState($ftp_input_ipadress, $GUI_HIDE)
	  GUICtrlSetState($Label3, $GUI_HIDE)
	  GUICtrlSetState($Label6, $GUI_HIDE)
	  GUICtrlSetState($ftp_input_folder, $GUI_HIDE)
	  GUICtrlSetState($ftp_input_linkurl, $GUI_HIDE)
	  
	  If $SettingsFile[3] == 1 Then
		 GUICtrlSetState($ftp_module, $GUI_CHECKED)
		 GUICtrlSetState($Group1, $GUI_SHOW)
		 GUICtrlSetState($ftp_input_username, $GUI_SHOW)
		 GUICtrlSetState($Label5, $GUI_SHOW)
		 GUICtrlSetState($Label4, $GUI_SHOW)
		 GUICtrlSetState($ftp_input_password, $GUI_SHOW)
		 GUICtrlSetState($ftp_input_ipadress, $GUI_SHOW)
		 GUICtrlSetState($Label3, $GUI_SHOW)	
		 GUICtrlSetState($Label6, $GUI_SHOW)
		 GUICtrlSetState($ftp_input_folder, $GUI_SHOW)
		 GUICtrlSetState($ftp_input_linkurl, $GUI_SHOW)
		 
	  EndIf
	  
	  $savefile = GUICtrlCreateButton("save", 77, 486, 134, 26)
	  GUISetState(@SW_SHOW)
   #EndRegion ### END Koda GUI section ###
   While 3
	  Switch GUIGetMsg()	
		 Case $GUI_EVENT_CLOSE
			GUIDelete($settingsform)
			Sleep(200)
			AdlibRegister ("Main", 250)
			AboutWindow()
		 Case $ftp_module  
			If _IsChecked($ftp_module) Then		
			   GUICtrlSetState($Group1, $GUI_SHOW)	   
			   GUICtrlSetState($ftp_input_username, $GUI_SHOW)
			   GUICtrlSetState($Label5, $GUI_SHOW)
			   GUICtrlSetState($Label4, $GUI_SHOW)
			   GUICtrlSetState($ftp_input_password, $GUI_SHOW)
			   GUICtrlSetState($ftp_input_ipadress, $GUI_SHOW)
			   GUICtrlSetState($Label3, $GUI_SHOW)	
			   GUICtrlSetState($Label6, $GUI_SHOW)
			   GUICtrlSetState($ftp_input_folder, $GUI_SHOW)
			   GUICtrlSetState($ftp_input_linkurl, $GUI_SHOW)
			Else
			   GUICtrlSetState($Group1, $GUI_HIDE)
			   GUICtrlSetState($ftp_input_username, $GUI_HIDE)
			   GUICtrlSetState($Label5, $GUI_HIDE)
			   GUICtrlSetState($Label4, $GUI_HIDE)
			   GUICtrlSetState($ftp_input_password, $GUI_HIDE)
			   GUICtrlSetState($ftp_input_ipadress, $GUI_HIDE)
			   GUICtrlSetState($Label3, $GUI_HIDE)	
			   GUICtrlSetState($Label6, $GUI_HIDE)
			   GUICtrlSetState($ftp_input_folder, $GUI_HIDE)      
			   GUICtrlSetState($ftp_input_linkurl, $GUI_HIDE)      
			EndIf
			
		 Case $openfolderbrowser
			Local $sMessage = "Select a folder to save your screenshots"
			
			Local $sFileSelectFolder = FileSelectFolder($sMessage, "", 1)
			If @error Then
			   $sFileSelectFolder = $SettingsFile[0]
			EndIf			
			$SettingsFile[0] = $sFileSelectFolder
			GUICtrlSetData($folderpath, $sFileSelectFolder)			
		 Case $savefile	
			If _IsChecked($ftp_module) Then
			   
			   $ftp_ipadress = GUICtrlRead($ftp_input_ipadress)
			   $ftp_username = GUICtrlRead($ftp_input_username)
			   $ftp_pass = GUICtrlRead($ftp_input_password)	
			   $ftp_folder = GUICtrlRead($ftp_input_folder)	
			   $ftp_link = GUICtrlRead($ftp_input_linkurl)	
			   
			   If IsString($ftp_ipadress) & IsString($ftp_username) & IsString($ftp_pass) & IsString($ftp_folder) & IsString($ftp_link) Then

				  SplashTextOn("FTP Module", "testing ftp connection ...", -1, 50, -1, -1, 4, "", 16)
				  Sleep(2000)
				  
				  
				  $Open = _FTPOpen('FTP_TEST')
				  $Conn = _FTPConnect($Open, $ftp_ipadress, $ftp_username, $ftp_pass)
				  If @error Then				  
					 SplashOff()
					 $Ftpc = _FTPClose($Open)
					 MsgBox(0+48, "FTP-Module", "can't connect to ftp!")
				  Else			 
					 $Ftpc = _FTPClose($Open) 
					 SplashOff()
					 SplashTextOn("FTP Module", "succesfuly connected to your FTP", -1, 50, -1, -1, 4, "", 16)
					 
					 $SettingsFile[3] = 1
					 $SettingsFile[4] = $ftp_ipadress
					 $SettingsFile[5] = $ftp_username
					 $SettingsFile[6] = $ftp_pass
					 $SettingsFile[7] = $ftp_folder
					 $SettingsFile[8] = $ftp_link
					 
					 IniWrite( $ProgrammSettingsFile, "ftp", "savepicture", $SettingsFile[0])
					 IniWrite( $ProgrammSettingsFile, "core", "showcapturewindow", $SettingsFile[1]) 
					 IniWrite( $ProgrammSettingsFile, "core", "autostart", $SettingsFile[2])     
					 IniWrite( $ProgrammSettingsFile, "ftp", "module", $SettingsFile[3])   
					 IniWrite( $ProgrammSettingsFile, "ftp", "ip", $SettingsFile[4])   
					 IniWrite( $ProgrammSettingsFile, "ftp", "username", $SettingsFile[5])   
					 IniWrite( $ProgrammSettingsFile, "ftp", "password", $SettingsFile[6])   
					 IniWrite( $ProgrammSettingsFile, "ftp", "folder", $SettingsFile[7])  
					 IniWrite( $ProgrammSettingsFile, "ftp", "linkurl", $SettingsFile[8])  
					 
					 Sleep(2000)
					 SplashOff()
					 
				  EndIf				  
			   Else
				  MsgBox(0+16, "FTP-Module", "please enter all input box")
			   EndIf
			EndIf
			If _IsChecked($autostart) Then
			   $SettingsFile[2] = 1			
			   _RegAddAdmin("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" , "Screenshot_Saver" , "REG_SZ" , @ScriptFullPath)
			Else
			   $SettingsFile[2] = 0
			   RegDelete("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "Screenshot_Saver")
			EndIf			
			$SettingsFile[1] = GUICtrlRead($interval)
			Sleep(200)			
			IniWrite( $ProgrammSettingsFile, "core", "savepicture", $SettingsFile[0])
			IniWrite( $ProgrammSettingsFile, "core", "showcapturewindow", $SettingsFile[1]) 
			IniWrite( $ProgrammSettingsFile, "core", "autostart", $SettingsFile[2]) 
			GUIDelete($settingsform)
			
			Sleep(200)
			AdlibRegister ("Main", 250)
			Sleep(200)
			
			__LoadConfig()
			AboutWindow() 
	  EndSwitch
	  SwitchCaseTray()  
   WEnd
   
EndFunc


;___________________________________________________________________

