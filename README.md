# Screenshot Saver v3.0


## Beschreibung
Mit diesem Windows Tool könnt ihr ganz leicht Screenshots von eurem Desktop erstellen.
Einfach ein drücken auf die Drucken-Taste und das Tool macht automatisch ein Screenshot
von deinem Desktop und speichert diese automatisch ab.

Falls aktiviert wird der Screenshot automatisch auf einen eingestellten FTP-Server hochgeladen.
Der öffentlich zugängliche Link wird automatisch in die Zwischenablage gespeichert.

Das Tool läuft im hintergrund.

## Screenshots

tray nothication

![Main Window](http://www.just4gaming.de/flo/__screenshots/main.JPG)


Einstellungen

![Settings Window](http://www.just4gaming.de/flo/__screenshots/settings.JPG)
