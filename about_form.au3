#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#Region ### START Koda GUI section ### Form=about.kxf
$about = GUICreate("About", 615, 249, -1, -1, -1, BitOR($WS_EX_TOOLWINDOW,$WS_EX_WINDOWEDGE))
$Icon1 = GUICtrlCreateIcon("D:\Source - Programme\Screenshot Saver\about.ico", -1, 42, 137, 32, 32)
$Label1 = GUICtrlCreateLabel("Screenshot Saver ", 105, 128, 166, 29)
GUICtrlSetFont(-1, 15, 400, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xB6181D)
$Label2 = GUICtrlCreateLabel("Developer: Florian 'Concil' Schmidt", 115, 159, 167, 17)
$Pic1 = GUICtrlCreatePic("C:\Users\Concilchen\Desktop\Designs\justin_banner.jpg", 0, 0, 615, 102)
$Label3 = GUICtrlCreateLabel("Website & Support: ", 115, 179, 98, 17)
$Label4 = GUICtrlCreateLabel("www.just4gaming.de", 213, 178, 120, 17)
GUICtrlSetFont(-1, 8, 800, 4, "MS Sans Serif")
GUICtrlSetColor(-1, 0x0000FF)
GUICtrlSetCursor (-1, 0)
$okay_button = GUICtrlCreateButton("Okay", 14, 214, 85, 25)
$settings = GUICtrlCreateButton("change Save folder", 121, 214, 134, 25)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit

		Case $Label4
		Case $okay_button
		Case $settings
	EndSwitch
WEnd
